package me.hguimaraes.tweets.TwitterHandler;

import twitter4j.Status;

/**
 * @author Hguimaraes
 */
public class Tweet {
    // Tweet info
    String msg;
    String date;
    String source;
    boolean isRetweeted;
	
    // User info
    int followers;
    long user_id;
    
    public Tweet(Status status){
        this.msg = status.getText();
	this.date = status.getCreatedAt().toString();
	this.source = status.getSource();
	this.isRetweeted = status.isRetweet();
	this.followers = status.getUser().getFollowersCount();
	this.user_id = status.getUser().getId();
    }
}
