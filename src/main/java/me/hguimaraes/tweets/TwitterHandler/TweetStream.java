package me.hguimaraes.tweets.TwitterHandler;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * @author Hguimaraes
 */
public class TweetStream {
    TwitterStream twitterStream = null;
    Tweet tweet = null;
    public Integer tweetCounter = 0;
    
    public TweetStream(){
        // Authentication in twitter
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey("CONSUMER-KEY-HERE")
            .setOAuthConsumerSecret("CONSUMER-SECRET-HERE")
            .setOAuthAccessToken("ACCESS-TOKEN-HERE")
            .setOAuthAccessTokenSecret("ACCESS-SECRET-HERE");

        // Initiate twitter stream using authentication
        this.twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();
        
        // Create a listener for Twitter events on the Stream API
        this.twitterStream.addListener(new StatusListener () {
            @Override
            public void onStatus(Status status) {
                tweetCounter++;
                saveTweet(status);
            }
            
            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
            
            @Override
            public void onDeletionNotice(StatusDeletionNotice arg0) {}
            
            @Override
            public void onScrubGeo(long arg0, long arg1) {}
            
            @Override
            public void onTrackLimitationNotice(int arg0) {}
            
            @Override
            public void onStallWarning(StallWarning arg0) {}
        });
    }
    
    public FilterQuery constructFilterQuery(String topic){
        FilterQuery tweetFilterQuery = new FilterQuery(); 
        tweetFilterQuery.track(new String[]{topic});
        tweetFilterQuery.language(new String[]{"en"});
        return tweetFilterQuery;
    }
    
    public TwitterStream getInstance(){
        return this.twitterStream;
    }
    
    private void saveTweet(Status status){
        this.tweet = new Tweet(status);
        Gson gson = new Gson();
        String line = gson.toJson(this.tweet);
        System.out.println(line);
    
        try (BufferedWriter bw = new BufferedWriter(
                new FileWriter("tweets.json", true)))
        {
            bw.write(line + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
