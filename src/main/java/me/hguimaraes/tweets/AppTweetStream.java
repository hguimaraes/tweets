package me.hguimaraes.tweets;

import java.util.Timer;
import java.util.TimerTask;
import me.hguimaraes.tweets.TwitterHandler.TweetStream;
import twitter4j.FilterQuery;
import twitter4j.TwitterStream;

/**
 * @author Hguimaraes
 */
public class AppTweetStream {
    public static void main(String[] args){
        //  Need to pass the keyword for search and windows size, in minutes,
        // for data collection.
        if(args.length < 2){
            System.out.println("Usage: tweet <trending_topic> <window_size_min>");
            System.exit(0);
        }
        
        // Received parameters from Command Line
        String search_theme = args[0];
        int time_minutes = Integer.parseInt(args[1]);
        
        // TwitterStream object to start listening for events
        TweetStream tweetstream = new TweetStream();
        FilterQuery tweetFilterQuery = tweetstream.constructFilterQuery(search_theme);
        TwitterStream twitterStream = tweetstream.getInstance();
        
        // Start listening to tweets using our filter!
        twitterStream.filter(tweetFilterQuery);
        
        // Finish the stream after N seconds
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Closing the app...");
                System.out.println("Number of saved tweets: " + 
                    Integer.toString(tweetstream.tweetCounter));
                twitterStream.shutdown();
            }
        }, time_minutes*60*1000);
    }
}
